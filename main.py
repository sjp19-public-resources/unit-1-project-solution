from inventory_manager import InventoryManager
import shlex


def main():
    manager = InventoryManager()

    while True:
        command = (
            input('Enter command (or "help" for available commands): ').strip().lower()
        )

        if command == "help":
            manager.help_command()
        elif command == "show_inventory":
            manager.show_inventory()
        elif command == "show_inventory_by_category":
            manager.show_inventory(by_category=True)
        elif command.startswith("search_inventory"):
            args = shlex.split(command)
            if len(args) == 2:
                keyword = args[1]
                results = manager.search_inventory(keyword)
                for item in results:
                    print(item)
            else:
                print("Usage: search_inventory <keyword>")
        elif command == "create_new_item":
            name = input("Enter name: ")
            category = input("Enter category (optional): ") or None
            cost = float(input("Enter cost: "))
            price = float(input("Enter price: "))
            quantity = int(input("Enter quantity: "))
            manager.create_new_item(name, category, cost, price, quantity)
        elif command.startswith("update_quantity"):
            args = shlex.split(command)
            if len(args) == 3:
                name = args[1]
                quantity = int(args[2])
                manager.update_quantity(name, quantity)
            else:
                print("Usage: update_quantity <name> <quantity>")
        elif command.startswith("remove_item"):
            args = shlex.split(command)
            if len(args) == 2:
                name = args[1]
                manager.remove_item(name)
            else:
                print("Usage: remove_item <name>")
        elif command == "exit":
            break
        else:
            print('Unknown command. Type "help" for available commands.')


if __name__ == "__main__":
    main()
