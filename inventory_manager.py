import json
import io


class InventoryManager:
    DEFAULT_JSON_FILE = "inventory.json"

    def __init__(self, json_file=DEFAULT_JSON_FILE):
        self.json_file = json_file

    def load_inventory(self):
        """Load inventory from the JSON file."""
        try:
            with io.open(self.json_file, "r", encoding="utf-8") as jsonfile:
                return json.load(jsonfile)
        except FileNotFoundError:
            print(f"{self.json_file} not found. Starting with an empty inventory.")
            return []

    def write_inventory(self, inventory):
        """Write the inventory to the JSON file."""
        with io.open(self.json_file, "w", encoding="utf-8") as outfile:
            json.dump(inventory, outfile, indent=4, ensure_ascii=False)

    def create_new_item(self, name, category, cost, price, quantity):
        """Create a new item in the inventory."""
        inventory = self.load_inventory()
        item = {
            "name": name,
            "category": category,
            "cost": cost,
            "price": price,
            "quantity": quantity,
        }
        inventory.append(item)
        self.write_inventory(inventory)

    def update_quantity(self, name, quantity):
        """Update the quantity of an item based on its name.
        Removes the item if the quantity is zero.
        """
        inventory = self.load_inventory()
        for item in inventory:
            if item["name"].lower() == name.lower():
                if quantity > 0:
                    item["quantity"] = quantity
                    print(f"Updated {name} to quantity: {quantity}")
                else:
                    inventory.remove(item)
                    print(f"Removed {name} from inventory as quantity is zero")
                self.write_inventory(inventory)
                return
        print("Item not found")

    def show_inventory(self, by_category=False):
        """Show inventory items, optionally grouped by category."""
        inventory = self.load_inventory()
        if by_category:
            category_inventory = {}
            for item in inventory:
                category = item["category"]
                if category not in category_inventory:
                    category_inventory[category] = []
                category_inventory[category].append(item)
            for category, items in category_inventory.items():
                print(f"Category: {category}")
                for item in items:
                    print(item)
        else:
            for item in inventory:
                print(item)

    def search_inventory(self, keyword):
        """Search inventory items by a keyword in their name."""
        inventory = self.load_inventory()
        return [item for item in inventory if keyword.lower() in item["name"].lower()]

    def remove_item(self, name):
        """Remove an item from the inventory by name."""
        inventory = self.load_inventory()
        for item in inventory:
            if item["name"].lower() == name.lower():
                inventory.remove(item)
                print(f"Removed {name} from inventory")
                self.write_inventory(inventory)
                return
        print("Item not found")

    def help_command(self):
        """Show a list of available commands."""
        commands = [
            "help: Shows available commands",
            "show_inventory: Displays all inventory items",
            "show_inventory_by_category: Displays inventory items grouped by category",
            "search_inventory <keyword>: Searches for items by keyword in name",
            "create_new_item: Adds a new item to inventory",
            "update_quantity <name> <quantity>: Updates the quantity of an item",
            "remove_item <name>: Removes an item by name",
            "exit: Exit the program",
        ]
        for command in commands:
            print(command)
